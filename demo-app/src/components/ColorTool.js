import React from 'react';

import { ToolHeader } from './ToolHeader';
import { ColorForm } from './ColorForm';

export class ColorTool extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      colors: props.colors.concat(),
    };
  }

  addColor = (newColor) => {
    this.setState({
      colors: this.state.colors.concat(newColor),
    });
  };



  render() {
    console.log('render');
    return <>
      <ToolHeader headerText="Color Tool" />
      <ul>
        {this.state.colors.map((color, index) =>
          <li key={index}>{color}</li>)}
      </ul>
      <ColorForm buttonText="Add Color" onSubmitColor={this.addColor} />
    </>;
  }
}