Exercise 7

1. Utilize the new ToolHeader component in Car Tool.

2. Create a new component named CarTable. Move the car table logic from CarTool to the CarTable component. Pass the cars from CarTool into CarTable via props.

3. Utilize the new CarTable inside of CarTool.

4. Create a new component named ViewCarRow. The view car row will display a row of car data. Utilize the ViewCarRow component in CarTable component to display the car data.

5. Ensure it works.

Bonus: Create a new component named UnorderedList which accepts and array of items (strings) and displays the list. Utilize the UnorderedList in Color Tool replacing the current list.