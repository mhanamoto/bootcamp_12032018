Exercise 10

1. Create a component named EditCarRow. The edit car row will receive a car and display input fields for each column of data (except car id, car id is just displayed as normal). The action column will have two buttons: Save and Cancel. When save is clicked the changes are saved to the array, when cancel is clicked the changes are not saved.

2. Add a new button to ViewCarRow with a label of Edit. When the Edit button is clicked, the ViewCarRow for that car is changed to EditCarRow. Only one row is editable at a time.

3. When the Add Car, Edit (of another row), Delete, Save or Cancel buttons are clicked and a row is being edited that row is changed back to a view row.

4. Ensure it works.