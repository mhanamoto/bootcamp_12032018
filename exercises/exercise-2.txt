Exercise 2

1. Using the content provided to you by your classmate and construct a web page

2. In the web page use the following tags:

h1, h2, header, footer, ul/li, p, blockquote, ask the person for a picture and display the picture using the img tags

You are free to use other tags if you would like in addition to the ones above

3. Ensure it displays correctly in the web browser