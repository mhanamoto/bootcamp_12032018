import fetch from 'node-fetch';
// import { PubSub } from 'graphql-subscriptions';

// const pubsub = new PubSub();

export const resolvers = {
  Query: {
    message: () => 'Hello World!',
    widgets: (_1, _2, { restURL }) => fetch(`${restURL}/widgets`).then(res => res.json()),
  },
  Mutation: {
    appendWidget: (_, { widget }, { restURL }) =>
      fetch(`${restURL}/widgets`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(widget)
      }).then(res => res.json()),
    deleteWidget: (_, { widgetId }, { restURL }) =>
      fetch(`${restURL}/widgets/${encodeURIComponent(widgetId)}`, {
        method: 'DELETE',
      }).then(() => widgetId),
  },
};
